﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator1
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrWhiteSpace(numbers))
            {
                return 0;
            }

            var delimiters = GetDelimiters(numbers);
            var numbersArray = GetNumbers(delimiters, numbers);

            ValidateNumbers(numbersArray);

            return GetSum(numbersArray);
        }

        private string[] GetDelimiters(string numbers)
        {
            string delimiter;
            string[] delimiters;

            if (numbers.StartsWith("//[") && numbers.Contains("]\n"))
            {
                delimiter = numbers.Substring(numbers.IndexOf("//[") + 2, numbers.IndexOf("]\n") - 1);
                delimiter = delimiter.Remove(delimiter.LastIndexOf("]"), 1).Remove(0, 1);
                delimiters = delimiter.Split(new[] { "][" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            }
            else if (numbers.StartsWith("//"))
            {
                delimiter = numbers.Substring(numbers.IndexOf("//") + 2, numbers.IndexOf("\n") - 2);
                delimiters = new[] { delimiter };
            }
            else
            {
                delimiters = new[] { "\n", "," };
            }

            return delimiters;
        }

        private int[] GetNumbers(string[] delimiters, string numbers)
        {
            try
            {
                if (!delimiters[0].Equals("\n"))
                {
                    numbers = numbers.Substring(numbers.IndexOf("\n") + 1);
                }

                var numbersArray = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                return numbersArray.Select(int.Parse).ToArray();
            }
            catch (Exception)
            {
                throw new Exception("Bad string");
            }
        }

        private void ValidateNumbers(int[] numbersArray)
        {
            List<int> negativeNumbers = new List<int>();

            foreach (var item in numbersArray)
            {
                if(item < 0)
                {
                    negativeNumbers.Add(item);
                }
            }

            if (negativeNumbers.Count != 0)
            {
                throw new Exception("Negatives not allowed " + string.Join(", ", negativeNumbers));
            }
        }

        private int GetSum(int[] numbersArray)
        {
            var sum = numbersArray[0];
            for (var i = 1; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] < 1001)
                {
                    sum += numbersArray[i];
                }
            }

            return sum;
        }
    }
}
