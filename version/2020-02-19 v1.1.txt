char[] specialCharacters = numbers.Where(aChar => !char.IsDigit(aChar) && aChar != '-').ToArray();
int[] numbersArray =  numbers.Split(specialCharacters).Where(number => !string.IsNullOrWhiteSpace(number)).Select(int.Parse).ToArray();
int[] negativeNumbersArray = numbersArray.Where(digit => digit < 0).ToArray();
          
if (negativeNumbersArray.Length != 0)
{
    throw new Exception("Negatives not allowed " + string.Join(", ", negativeNumbersArray));
}
return numbersArray.Where(digit => digit <= 1000).Sum();